package pl.sdaacademy.tetris;

public class Square extends Figure {
    public Square(Direction direction, Point center) {
        super(direction, center);
    }

    @Override
    void setPoints() {
        points[0] = center;
        points[1] = new Point(center.getX() + 1, center.getY());
        points[2] = new Point(center.getX(), center.getY() + 1);
        points[3] = new Point(center.getX() + 1, center.getY() + 1);

    }
}
