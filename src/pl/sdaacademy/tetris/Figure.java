package pl.sdaacademy.tetris;

public abstract class Figure {
    Direction direction;
    Point[] points = new Point[4];
    Point center;
    private PaintStrategy paintStrategy;

    public Figure(Direction direction, Point center) {
        this.center = center;
        this.direction = direction;
        setPoints();
    }

    public Point[] getPoints() {
        return points;
    }

    abstract void setPoints();

    public Figure setDirection(Direction direction) {
        this.direction = direction;
        setPoints();
        return this;
    }

    public Figure setPaintStrategy(PaintStrategy paintStrategy) {
        this.paintStrategy = paintStrategy;
        return this;
    }

    public void move(Direction direction) {
        switch (direction) {
            case HORIZONTAL1:
                center = new Point(center.getX() + 1, center.getY());
                break;
            case HORIZONTAL:
                center = new Point(center.getX() - 1, center.getY());
                break;
            case VERTICAL1:
            case VERTICAL:
                center = new Point(center.getX(), center.getY() + 1);
                break;
        }
        this.setPoints();
    }

    public void turn(MoveDirection moveDirection) {
        switch (moveDirection) {
            case LEFT:
                this.setDirection(leftDirection());
                break;
            case RIGHT:
                this.setDirection(rightDirection());
                break;
        }


        this.setPoints();
    }

    protected Direction rightDirection() {
        switch (this.direction) {
            case VERTICAL:
                return Direction.HORIZONTAL1;

            case HORIZONTAL:
                return Direction.VERTICAL;

            case VERTICAL1:
                return Direction.HORIZONTAL;

            case HORIZONTAL1:
                return Direction.VERTICAL1;

        }
        return null;
    }

    private Direction leftDirection() {
        switch (this.direction) {
            case VERTICAL:
                return Direction.HORIZONTAL;

            case HORIZONTAL:
                return Direction.VERTICAL1;

            case VERTICAL1:
                return Direction.HORIZONTAL1;

            case HORIZONTAL1:
                return Direction.VERTICAL;

        }
        return null;
    }

    public Point getCenter() {
        return center;
    }

    public Figure setCenter(Point center) {
        this.center = center;
        this.setPoints();
        return this;
    }

    public void moveHorizontally(MoveDirection moveDirection) {
        int x = 0;
        switch (moveDirection) {

            case LEFT:
                x = center.getX() - 1;

                break;
            case RIGHT:
                x = center.getX() + 1;


                break;
        }
        center.setX(x);
        this.setPoints();
    }
}
