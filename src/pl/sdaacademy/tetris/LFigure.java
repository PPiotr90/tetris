package pl.sdaacademy.tetris;

public class LFigure extends Figure {

    public LFigure(Direction direction, Point center) {
        super(direction, center);
    }


    @Override
    void setPoints() {
        switch (direction) {
            case VERTICAL:
                points[0] = new Point(center.getX() + 1, center.getY());
                points[1] = center;
                points[2] = new Point(center.getX(), center.getY() - 1);
                points[3] = new Point(center.getX(), center.getY() - 2);
                break;

            case HORIZONTAL:
                points[0] = new Point(center.getX(), center.getY() + 1);
                points[1] = center;
                points[2] = new Point(center.getX() + 1, center.getY());
                points[3] = new Point(center.getX() + 2, center.getY());

                break;

            case VERTICAL1:
                points[0] = new Point(center.getX() - 1, center.getY());
                points[1] = center;
                points[2] = new Point(center.getX(), center.getY() + 1);
                points[3] = new Point(center.getX(), center.getY() + 2);

                break;
            case HORIZONTAL1:
                points[0] = new Point(center.getX(), center.getY() - 1);
                points[1] = center;
                points[2] = new Point(center.getX() + 1, center.getY());
                points[3] = new Point(center.getX() + 2, center.getY());

                break;
        }
    }
}
