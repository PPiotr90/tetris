package pl.sdaacademy.tetris;

public class Long extends Figure {


    public Long(Direction direction, Point center) {
        super(direction, center);

    }

    @Override
    void setPoints() {
        switch (super.direction) {
            case VERTICAL1:
            case VERTICAL:
                for (int i = 0; i < 4; i++) {
                    points[i] = new Point(center.getX(), center.getY() - 1 + i);
                }
                break;
            case HORIZONTAL1:
            case HORIZONTAL:
                for (int i = 0; i < 4; i++) {
                    points[i] = new Point(center.getX() - 1 + i, center.getY());
                }
                break;
        }
    }
}
