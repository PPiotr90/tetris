package pl.sdaacademy.tetris;

import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.paint.Color;

public class Controller {
    @FXML
    Button button1;
    @FXML
    Button button2;
    @FXML
    Button button7;
    @FXML
    Canvas canvas;
    @FXML
    Slider slider;
    Tetris tetris;
    GraphicsContext graphicsContext2D;
    int POINT_SIZE = 20;
    Thread thread = new Thread(() -> {
        tetris.start();
        tetris.show();
    });

    public void initialize() {
        slider.setMax(20);
        slider.setMin(1);
        slider.setValue(1);
        canvas.resize(400, 400);
        tetris = new Tetris(10, 20);
        tetris.setRealSpeedCounter(() -> (int) (tetris.getBaseSpeed() / slider.getValue()));
        graphicsContext2D = canvas.getGraphicsContext2D();

        tetris.setPaintStrategy(() -> {
            canvas.getGraphicsContext2D().clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
            for (Point point : tetris.getFigure().getPoints()) {
                drawPoint(point.getX(), point.getY(), Color.AQUA);

            }
            for (Point point : tetris.getSolid()) {
                drawPoint(point.getX(), point.getY(), Color.OLIVE);
            }
        });
        button1.setOnAction(event -> tetris.moveHorizontally(MoveDirection.LEFT));
        button2.setOnAction(event -> tetris.moveHorizontally(MoveDirection.RIGHT));

        button7.setOnAction(event -> {
            tetris.turn(MoveDirection.LEFT);
            tetris.show();
        });


        thread.start();

    }

    private void drawPoint(int x, int y, Color color) {

        if (inBounds(x, y)) {
            graphicsContext2D.setFill(color);
            graphicsContext2D.fillRect(x * POINT_SIZE, y * POINT_SIZE, POINT_SIZE, POINT_SIZE);
        }
    }

    public boolean inBounds(int x, int y) {
        return x >= 0 && x < tetris.getxBounds()
                && y >= 0 && y < tetris.getyBounds();
    }
}
