package pl.sdaacademy.tetris;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Tetris {
    private final int xBounds;
    private final int yBounds;
    private final int baseSpeed = 1000;
    public ArrayList<Point> solid = new ArrayList<>();
    PaintStrategy paintStrategy;
    Point begining;
    MoveDirection mustMoveTo;
    RealSpeedCounter realSpeedCounter;
    private Figure figure;
    private Point[][] platform;
    private int realSpeed = 1000;

    public Tetris(int xBounds, int yBounds) {
        this.xBounds = xBounds;
        this.yBounds = yBounds;
        platform = new Point[xBounds][yBounds];
        for (int i = 0; i < xBounds; i++) {
            solid.add(new Point(i, yBounds));
        }
        begining = new Point((int) (xBounds / 2), 0);
        figure = randomFigure(begining);

    }

    public Figure getFigure() {
        return figure;
    }

    public Tetris setFigure(Figure figure) {
        this.figure = figure;
        return this;
    }

    public Tetris setPaintStrategy(PaintStrategy paintStrategy) {
        this.paintStrategy = paintStrategy;
        return this;
    }

    public void setDirection(Direction direction) {
        figure.setDirection(direction);
    }

    public void show() {
        if (paintStrategy != null) {
            paintStrategy.paint();
        }
    }

    public int getxBounds() {
        return xBounds;
    }

    public int getyBounds() {
        return yBounds;
    }

    public void move() {
        if (nextIsSolid()) {
            solid.addAll(Arrays.asList(figure.getPoints()));
            figure = randomFigure(begining);
        } else
            figure.move(Direction.VERTICAL);
    }


    public boolean nextIsSolid() {
        boolean result = false;
        for (Point point : figure.getPoints()) {
            if (solid.contains(new Point(point.getX(), point.getY() + 1))) {
                result = true;
                break;
            }
        }
        return result;
    }

    public void start() {
        while (!isEnd()) {
            this.move();
            this.show();
            this.clearFullStrip();
            this.calcRealSpeed();

            try {
                Thread.sleep(realSpeed);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void clearFullStrip() {
        ArrayList<Integer> checked = new ArrayList<>();
        ArrayList<Point> copyOfSolid = new ArrayList<>(solid);
        for (Point point : solid) {
            int y = point.getY();

            if (y < yBounds && !(checked.contains(y))) {
                List<Point> row = Arrays.asList(allPointsFromRow(y));
                checked.add(y);
                if (copyOfSolid.containsAll(row)) {
                    copyOfSolid.removeAll(row);
                    for (Point point1 : copyOfSolid) {
                        if (point1.getY() < y) {
                            point1.setY(point1.getY() + 1);
                        }
                    }
                }
            }

        }
        solid = copyOfSolid;
        show();

    }

    private Point[] allPointsFromRow(int y) {
        Point[] result = new Point[xBounds];
        for (int i = 0; i < xBounds; i++) {
            result[i] = new Point(i, y);

        }
        return result;
    }


    private Figure randomFigure(Point center) {
        Random random = new Random();
        Direction direction;
        int d = (int) (4 * random.nextDouble());
        int f = (int) (6 * random.nextDouble());
        switch (d) {
            case 0:
                direction = Direction.VERTICAL;
                break;
            case 1:
                direction = Direction.HORIZONTAL;
                break;
            case 2:
                direction = Direction.VERTICAL1;
                break;
            default:
                direction = Direction.HORIZONTAL1;
                break;
        }
        switch (f) {
            case 0:
                return new Long(direction, center);

            case 1:
                return new ZFigure(direction, center);

            case 2:
                return new SFigure(direction, center);

            case 3:
                return new Square(direction, center);

            case 4:
                return new LFigure(direction, center);

            default:
                return new JFigure(direction, center);

        }
    }

    public ArrayList<Point> getSolid() {
        return solid;
    }


    private boolean isEnd() {
        boolean result = false;
        for (int i = 0; i < xBounds; i++) {
            Point point = new Point(i, -1);
            if (solid.contains(point)) {
                result = true;
                break;
            }


        }
        return result;
    }

    public void moveHorizontally(MoveDirection moveDirection) {
        MoveDirection opposite = oppositeDirection(moveDirection);
        Point p1 = figure.getPoints()[0];
        Point p2 = figure.getPoints()[3];
        figure.moveHorizontally(moveDirection);
        if (outOfXBounds()) {
            this.moveHorizontally(opposite);

        }
        if (inSolid()) {
            moveHorizontally(opposite);
        }
        this.show();
    }

    private MoveDirection oppositeDirection(MoveDirection moveDirection) {
        if ((moveDirection == MoveDirection.LEFT)) return MoveDirection.RIGHT;
        else return MoveDirection.LEFT;
    }

    public void turn(MoveDirection moveDirection) {
        figure.turn(moveDirection);
        if (outOfXBounds()) {
            this.moveHorizontally(mustMoveTo);
        }
        if (inSolid()) {
            turn(oppositeDirection(moveDirection));
        }
    }

    public boolean outOfXBounds() {
        Point p1 = figure.getPoints()[0];
        Point p2 = figure.getPoints()[3];
        if (p1.getX() >= xBounds || p2.getX() >= xBounds) {
            mustMoveTo = MoveDirection.LEFT;
            return true;

        } else if (p1.getX() < 0 || p2.getX() < 0) {
            mustMoveTo = MoveDirection.RIGHT;
            return true;

        }
        return false;
    }

    private boolean inSolid() {
        boolean result = false;
        for (Point point : figure.getPoints()) {
            if (solid.contains(point)) {
                result = true;
                break;
            }
        }
        return result;
    }

    public Tetris setRealSpeedCounter(RealSpeedCounter realSpeedCounter) {
        this.realSpeedCounter = realSpeedCounter;
        return this;
    }

    public int getBaseSpeed() {
        return baseSpeed;
    }

    public void calcRealSpeed() {
        if (realSpeedCounter != null) {
            this.realSpeed = realSpeedCounter.count();
        }
    }
}

