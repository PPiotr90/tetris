package pl.sdaacademy.tetris;

public interface PaintStrategy {
    public void paint();
}
