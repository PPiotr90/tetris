package pl.sdaacademy.tetris;

public class ZFigure extends Figure {

    public ZFigure(Direction direction, Point center) {
        super(direction, center);
    }

    @Override
    void setPoints() {
        switch (direction) {
            case HORIZONTAL:
            case HORIZONTAL1:
                points[0] = new Point(center.getX() - 1, center.getY());
                points[1] = center;
                points[2] = new Point(center.getX(), center.getY() + 1);
                points[3] = new Point(center.getX() + 1, center.getY() + 1);

                break;
            case VERTICAL:
            case VERTICAL1:
                points[0] = new Point(center.getX(), center.getY() - 1);
                points[1] = center;
                points[2] = new Point(center.getX() - 1, center.getY());
                points[3] = new Point(center.getX() - 1, center.getY() + 1);

                break;
        }
    }
}
